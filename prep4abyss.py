#!/usr/bin/env python3

# prep4abyss.py
# usage: python3 prep4abyss.py <list of fastq files>
# availability: https://gitlab.com/MachadoDJ/prep4abyss

import argparse, sys, re
from Bio import SeqIO

def main(args):
	for file in args.files:
		for record in SeqIO.parse(file, "fastq"):
			d = record.description
			p = re.compile(" ([12]):.+?:.+?:").findall(d)[0]
			record.id="{}/{}".format(record.id, p)
			record.description=""
			sys.stdout.write("{}".format(record.format("fastq")))
	return

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Collect a list of files from the user.")
	parser.add_argument("-f", "--files", nargs="+", help="List of files to process")
	args = parser.parse_args()
	main(args)
