# What is this script?

ABySS is a de novo sequence assembler intended for short paired-end reads and large genomes. ABySS is available [here](https://github.com/bcgsc/abyss). In ABySS, a pair of reads must be named with the suffixes `/1` and `/2` to identify the first and second read, or the reads may be named identically.



FastQ files from Casava 1.8 have headers that do not end in `/1` or `/2` (e.g., `@EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG` or `@EAS139:136:FC706VJ:2:2104:15343:197393 1:N:18:1`). This program changes these headers so that they will end with proper header suffix (e.g., `@EAS139:136:FC706VJ:2:2104:15343:197393/1`), as required by ABySS.



Note that this will erase the following information from the sequence header:

1. Information indicating whether the sequence has been filtered (`Y` or `N`)
2. Information indicating whether control bits are on (`0`; the field would otherwise have a even number)
3. Index sequence or output a sample number.

# How do I use this script?

## Command line

```bash
python3 prep4abyss.py input1.fastq input2.fastq ... inputN.fastq > output.fastq
```

## Before running the script

Please note that this script takes one or more input files in FastQ format, modifies their header, and prints the results to the standard output. It expects uncompressed files only. If the sequence header is not in Casava 1.8 format, an error message will show.

# Dependencies

This script was written for [Python3](https://www.python.org/downloads/) and requires [Biopython](https://biopython.org/wiki/Download).